# dhbw-cluster-dummy-service

This repository contains a dummy service which gets deployed to [dhbw-cluster](https://gitlab.com/wwi22amb/dhbw-cluster) Kubernetes cluster.

The deployed service is [mendhak/docker-http-https-echo](https://github.com/mendhak/docker-http-https-echo) which simply returns all the information of the incoming HTTP request.

It's running here: https://dhbw.gemueseoderobst.de/teamdummy/service

The deployment configuration is in `deployment.yml`.

To deploy, ensure that the `KUBECONFIG` environment variable points to your kubeconfig file. The configured user needs access to the `teamdummy-ns` namespace. Then simply run `kubectl apply -f deployment.yml`.

Future improvements:
- Automatically deploy the service through Gitlab CI.
- Configure the `cluster-ingress-minion` ingress to let it rewrite the request path of the HTTP request to the service. (for example: `/teamdummy/service` -> `/`), see [docs](https://kubernetes.github.io/ingress-nginx/examples/rewrite/).